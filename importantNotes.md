# HASKELL SLIDES

- 77 -> Type assignation bindings
- 130 -> Type decorations
- 131 -> Type generalization
- 133 -> MM algorithm for finding the MGU

# KINDS OF QUESTIONS:

## About Types

- Given a `data` definition and a function `f`, infer `f`s type:

  - DECORATION: `(f::a x::a0 Z::Nat)::a1 = (x::a0 Z::Nat []::[a2])::a1`
  - RESTRICTIONS: `(1) a = a0 -> Nat -> a1 (from lhs of eq1)`
  - SOLVE: `unifying (1) and (3) => a0 -> Nat -> a1 = a3 -> a5 -> a1`
  - GENERALIZE: `f:: Nat -> (Nat -> a1) -> a1`

- Given a function `f`, reason briefly about it's type:

  - How many parameters? `f:: t1 -> t2 -> t3 (2 params)`
  - What do those params bind to? `x::t1, y::t2, (result)::t3`
  - What can the function tell about the types? `if result is a list of x, then t3 = [t1]`
  - Can you unify any types? `if there is pattern matching in l/rhs`
  - Is it well-typed?

- Define a `data` type, and:

  - Implement certain operations.

    - If the type derives a class `C`, then you may have to declare it's type `(C a) => Type a -> Bool -> Bool` or something.
    - Write a short mathematical recursive definition!!
    - **Beware of the sign!!!**

  - Make it an instance of a certain class: `declara X como instancia de la clase C`

    - Declare it as an instance: `instance Ord Rational' where`

      - For **recursive** types we need context: `instance (Ord a) => Ord (Arbol a) where`

    - How does it differ from `deriving (Class)`? `Rational' has to compare simplified fractions`

    - If you have to declare it as an instance of Ord, it also has to be an instance of Eq

    - Redefine the operations: `frac <= frac' = (x, y) <= (x', y') -- Tuple comparison is in the standard`

    - Enforce bindings: `where Frac x y = simp frac, Frac x' y' = sim frac'`

      - Just like a `let x = e in e'`, we have `where e x = e'`

  - Enforce types inside it

    - `data Arbol' a b = Hoja' a | Nodo' b (Arbol' a b) (Arbol' a b)`

  - Constructors are just functions

    - `data Name <,types> = Constructor0 Type0 (can be predefined (Int) or declared in <,types>) Type1.. | Costructor1 ...`
    - if We have `data N = C Int Bool`:

      - We have to use it like this: `fun :: N -> N` `fun (C a b) = (C (exp a) (exp b))` Note the parenthesis.

## About Lists

- Define a function that, given a list, returns a list of lists:

  - All the subsets, the prefixes...
  - Usually done by concatenating intentional lists. `subs (x:xs) = [x:ys | ys <- subs xs] ++ subs xs`
  - Write first an informal _recursive_ definition, for every element:

    ```
    -- How does element x of xs affect the overall result?
    -- If we have a list of subsets, and we want to add another element x,
    -- the new subsets will be the original subsets PLUS all those subsets with x attatched
    ```

  - If you need to use `foldr` or `foldl`, remember this:

    - Schema `foldr (\elem acc -> expression with elem and acc) 0 xs`
    - Schema `foldl (\acc elem -> expression with elem and acc) 0 xs`
    - With lambdas is very easy, you can retrieve the acc and the elem

## Types and parenthesis

- `->` binds from the right. **Always read the types from the right!!!**:

  - A -> B -> C -> D

    - == A -> (B -> (C -> D))
    - == A -> B -> (C -> D)
    - != A -> (B -> C -> D)
    - != (A -> B) -> C -> D
    - != A -> (B -> C) -> D

- `functions` bind from the **left**:

  - `f x y z`

    - == `((f x) y) z`
    - == `(f x y) z`
    - == `(f x) y z`
    - != `f (x y z)`
    - != `f (x y) z`
    - != `f x (y z)`

- `infixr a f` == f is infix from the right, and has priority a (more is better)

## Prolog:

- Deduction trees:

  - Just have in mind that, for each step:

    - Pick a rule
    - For each variable A, make A{num} = unification with the lhs

    - If (A{num} unifies with (a constant or an original variable))

      - Substitute A{num} with the unification.

    - If (the rule has a rhs)

      - The new objective is the rhs.

    - Else

      - That objective is solved.

- Operating with lists

  - Do not forget the base cases! (They go **before** other cases)
  - `member(X, [X|Xs]).`, `member(X, [Y|Xs]) :- member(X, Xs)`
  - The predicates `number(X)` and `var(X)` are available!

- The cut:

  - If we have an expression that has a cut after it, we first check the branch on the left of the cut, then if it succeeds, we cut the others.
  - The order of the predicates is important!
  - Example:

    - `p(X) :- r(X), !, c(Y).`
    - `p(X) :- s(Z).`
    - If X=a succeeds, then we cut the rest of the expression, so the new rules become:

      - `p(X) :- r(a), !, c(Y)` (c(X) is not affected)
      - `null.` (We don't even do it)

- Meta Prolog:

  - `T =.. [Func|Args]` == T = Func([arg1, arg2...argn])
  - You usially define 3 cases: base, whatyoulookfor, otherstuff.

    - For each functor in the expression:

      - Pass the argument list to an auxiliary predicate
      - If it's what you looking for, then add it to the result

  - You also need an auxiliary predicate, that evaluates lists instead of simple functors:

    - For each element in the list:

      - Pass the head to the simple predicate (above). Keep the result (N1)
      - Pass the rest of the list to youtself. Keep the result (N2)
      - Add both results.

  - Example

  ```prolog
  veces(X,0) :- var(X),!.
  veces(T, N) :- T =.. [f|Args],!, vecesi(Args, N1), N is N1+1\.
  veces(T, N) :- T =.. [_|Ts], vecesi(Ts, N).

  vecesi([], 0).
  vecesi([X|Xs], N) :- veces(X, N1), vecesi(Xs, N2), N is N1 + N2.
  ```
