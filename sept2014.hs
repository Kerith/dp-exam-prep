-- Sept 2014

-- 1)
--  a/ Write an expression whose evaluation produces the value [(1, 1), (2, 4)...]
squares :: Int -> [(Int, Int)]
squares x = take x [(e, e*e) | e <- [1, 2 ..]]
-- Expression is `squares 20`

--  b/ Reason about the type of the following function
f x (y:ys) = (x y):f x ys

-- 3 arguments: f :: a0 -> a1 -> a2
-- Return type and param 2 are lists: f :: a0 -> [a1] -> [a2]
-- For the return type we can see that x is an application on y's type:
--    a0 = a1 -> a2 => f :: (a1 -> a2) -> [a1] -> [a2]
-- No more info can be extracted

-- 2)
--   a/ Define the type Fraccion that hold fractions of integers
data Fraccion = F Int Int deriving (Show)
--   b/ Implement the operations over Fraccion:
--      i/ Raise to an integer power
elevar :: Fraccion -> Int -> Fraccion
elevar (F a b) pow = (F (a^pow) (b^pow))
--      ii/ Calculate the arithmetic mean on a list of Fracciones
mean :: [Fraccion] -> Fraccion
mean xs = simpl $ mul (foldr (\(F a b) (F aacc bacc) -> (F (a*bacc+aacc*b) (b*bacc))) (F 0 1) xs) (F 1 (length xs))

simpl :: Fraccion -> Fraccion
simpl (F x0 y0) = (F (x0 `div` (gcd x0 y0)) (y0 `div` (gcd x0 y0)))

mul :: Fraccion -> Fraccion -> Fraccion
mul (F x0 y0) (F x1 y1) = (F (x0*x1) (y0*y1))
--   c/ Define Fraccion as an instance of the classes Eq and Ord
instance Eq Fraccion where
  f0 == f1 = a0 == a1 && b0 == b1
   where F a0 b0 = simpl f0
         F a1 b1 = simpl f1

instance Ord Fraccion where
  f0 <= f1 = f0 == f1 || f0 < f1
  f0 < f1 = (b0 == b1 && a0 < a1) || tr0 < tr1
    where F a0 b0 = simpl f0
          F a1 b1 = simpl f1
          tr0 = a0*b1
          tr1 = a1*b0

-- 3) Implement, indicating the types, the following functions.
--    a/ mappos f [x0, x1,...,xn] = [f 0 x0, f 1 x1,..., f n xn]
mappos :: (Int -> a -> b) -> [a] -> [b]
mappos f xs = [x | i <- [0..(length xs)], x <- (map (f i) xs)]
--    b/ g n = [[0,1,2,...n], [1,2...n], ... [n]]
--       for every i in [0...n], we have to
--       create a new list and add it to all the previous lists
g :: Int -> [[Int]]
g 0 = [[0]]
g n = [xs++[n] | xs <- (g (n-1))]++[[n]]
