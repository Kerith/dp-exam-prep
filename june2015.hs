-- June 2015

-- 1)

--   a/
f = 0:[i| j <- [1..], i <- [-j, j]]

--   b/

--   Tres argumentos: a0 -> a1 -> a2 -> a3
--   El primero es una lista: a0 = [a]
--   Por la eq1 dabemos que el tipo de y es el tipo devuelto: a2 = a3 = b
--   Por el rhs de la eq2 tenemos que x es una función de 2 argumentos:
--      a1 = (b -> a -> b)
--   Por lo tanto, tenemos que:
--      f :: [a] -> (b -> a -> b) -> b -> b

-- 2)
--   i/
data Figura = R (Int, Int) (Int, Int) | C (Int, Int) Int deriving (Eq, Ord)
--   ii/
--inter :: Figura -> Figura -> Bool
--inter (C (x, y) r) (C (x', y') r') = sqrt ((x' - x) ^ 2 + (y' - y) ^ 2) <= (r + r')
--inter (R (a0, b0) (a1, b1)) (R (a0', b0') (a1', b1')) = False

--   iii/
area :: Figura -> Float
area (C _ r) = 3.14 * (fromIntegral r) ^ 2
area (R (a0, b0) (a1, b1)) = fromIntegral ((a1 - a0) * (b1 - b0))

instance Eq Figura where
  f0 == f1 = area f0 == area f1

instance Ord Figura where
  f0 <= f1 = area f0 <= area f1

--    iv/
--    El orden sería el de los tuples de puntos, primero se compararían los elementos del primer tuple, y luego los del segundo. En el caso de comparar un circulo con un rectangulo, el círculo tendrá preferencia, dado que se define un int como mayor que cualquier tuple.

-- 3)
--    i/
apariciones :: [Int] -> [Int] -- WRONG! apariciones :: Eq a => [a] -> [Int]
apariciones xs = (map (\x -> length $ filter (== x) xs) xs)

--    ii/
divHasta :: Int -> [(Int, Int)]
divHasta n = [(x, length $ filter (\y -> (x`mod`y) == 0) [1..x]) | x <- [1..n]]

-- 5)
--   a/
separa([X|Xs], Ys, [X|Us], Vs) :- member(X, Ys), !, separa(Xs, Ys, Us, Vs).
separa([X|Xs], Ys, Us, [X|Vs]) :- separa(Xs, Ys, Us, Vs).
separa([], _, _, _).

member(X, [X|Xs]).
member(X, [Y|Ys]) :- member(X, Ys).


--   b/
