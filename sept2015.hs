-- September 2015

-- 1)
--    a/
l = [(x, (2^x)-1) | x <- [0..]]

--    b/
f x y []      = y
f x y (a:as)  = f x (x y a) as
--    f tiene 3 arumentos: f :: ax -> ay -> a2 -> a3
--    El tercer argumento es una lista: a2 = [a4]
--    ay es también el resultado de la función: a3 = ay
--    x es una aplicación sobre a e y, y devuelve del tipo de y: ax = (ay -> a4 -> ay)
--    Por lo tanto, f queda: f :: (ay -> a4 -> ay) -> ay -> [a4] -> ay
--    No hay mas restricciones, asi que se generaliza:
--      f :: (b -> a -> b) -> b -> [a] -> b

-- 2)
--    a/
data EBool = Cierto | Not EBool | And EBool EBool
--    b/
eval :: EBool -> Bool
eval (Cierto) = True
eval (Not x) = not (eval x)
eval (And x y) = (eval x) && (eval y)

--    c/

iguales :: EBool -> EBool -> Bool
iguales Cierto Cierto = True
iguales (Not x) (Not y) = iguales x y
iguales (And x y) (And x' y') = (iguales x x' && iguales y y') || (iguales x y' && iguales y x')
iguales x y = False

instance Eq EBool where
  e0 == e1 = iguales e0 e1

--    d/


--  3)
--     a/
-- Each element combines itself with every other subset
-- p [] => [[]]
-- p [1] => p [] ++ map (++[1]) (p [])
-- p [1, 2] = [[], [1]] ++ map (++[2]) [[], [1]]
partes :: [a] -> [[a]]
partes [] = [[]]
partes (x:xs) = partes xs ++ map (++[x]) (partes xs)

--     b/
sumSeg :: [Int] -> [Int]
sumSeg (x:xs) = reverse $ foldr (\e (ac:acs) -> if (e == 0) then (e:(ac:acs)) else ((ac+e):acs)) [x] xs

-- 4) (Paper)
--p(X,c(Y,Z)) :- p(X,Z).
--q(a).
--p(X,c(X,Y)).
--q(b).

-- 5)
--    a/
--separa([], _, 0, _).
--separa([X|Xs], X, N, Ys) :- separa(Xs, X, N1, Ys), N is N1 + 1.
--separa([Y|Xs], X, N, [Y|Ys]) :- separa(Xs, X, N, Ys), X \= Y.
