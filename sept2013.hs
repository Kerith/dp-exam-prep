-- Septiembre 2013

-- 1)
-- Given the HASKELL type:
data Nat = Z | S Nat
-- Infer the type of the following function:
f x Z = x Z []
f y (S x) = y x [x]

-- We have the definitions:
--   Z:: Nat, S:: Nat -> Nat

-- DECORATE
--  (f::a x::a0 Z::Nat)::a1 = (x::a0 Z::Nat []::[a2])::a1
--  (f::a y::a3 (S::Nat->Nat x::a4)::a5)::a1 = (y::a3 x::a4 [x]::[a4])::a1

-- GENERATE RESTRICTIONS
--   (1) a = a0 -> Nat -> a1 (from lhs of eq1)
--   (2) a0 = Nat -> [a2] -> a1 (from rhs of eq1)
--   (3) a = a3 -> a5 -> a1 (from lhs of eq2)
--   (4) Nat->Nat = a4 -> a5 (from the definition of a5)
--   (5) a3 = a4 -> [a4] -> a1 (from rhs of eq2)

-- SOLVE RESTRICTIONS
--   unifying (1) and (3) => a0 -> Nat -> a1 = a3 -> a5 -> a1
--   separating the unification => a0 = a3, a5 = Nat
--   unifying (4) and (a5 = Nat) => Nat -> Nat = a4 -> Nat
--   separating the unification => a4 = Nat
--   substituting (a4 = Nat) in 5 => a3 = Nat -> [Nat] -> a1
--   current set:
--     (1) a = a0 -> Nat -> a1
--     (2) a0 = Nat -> [a2] -> a1
--     (3) a = a0 -> Nat -> a1
--     (4) Nat -> Nat = Nat -> Nat
--     (5) a3 = Nat -> [Nat] -> a1
--   eliminating (3) and (4):
--     (1) a = a0 -> Nat -> a1
--     (2) a0 = Nat -> [a2] -> a1
--     (5) a3 = Nat -> [Nat] -> a1
--   substituting (2) in (1) => a = (Nat -> [a2] -> a1) -> Nat -> a1

-- GENERALIZE
-- We can assume that a2 = Nat, when unifying with (5).
-- Thus, the type is f :: (Nat -> [Nat] -> a1) -> Nat -> a1

-- 2)
--   1/ Define in HASKELL a type Tree a b to represent binary trees of type a
--      in the intermediate nodes, and type b in the leaves.


-- 3)
--   1/ Define in haskell the following function:
--      subs:: [a] -> [[a]]
--      subs xs = list of all the subsets of xs

-- If we have a list of subsets, and we want to add another element x,
-- the new subsets will be the original subsets PLUS all those subsets with x attatched
subs :: [a] -> [[a]]
subs [] = [[]]
subs (x:xs) = [x:ys | ys <- subs xs] ++ subs xs

--   2/ Implement in Haskell the following function, and indicate it's type:
--      suman n xs = there is a subset of elements in xs that add up to n (NP-C)
suman :: (Num a, Eq a) => a -> [a] -> Bool
suman n xs = head [x | x <- subs xs, (sum x) == n] /= []
