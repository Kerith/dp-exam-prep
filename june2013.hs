-- Junio 2013

-- 1) OK
-- Given the following definitions
data Nat = Z | S Nat

f:: Nat -> (Nat -> a1) -> a1
f Z x = x Z
f (S x) g = f x g

-- Infer the type of f

-- We can assume that:
--  Z :: Nat
--  S :: Nat -> Nat

-- DECORATION: Assign types to each expression:
--  (f::a Z::Nat x::a0)::a1 = (x::a0 Z::Nat)::a1
--  (f::a (S::Nat->Nat x::a2)::a3 g::a4)::a1 = (f::a x::a2 g::a4)::a1
--    Assign the most generic types possible, assuming that we
--    Already know the type of Z and S, and both sides of an expression
--    Have the same type, and both equations of a function have the same type

-- GENERATION OF TYPE RESTRICTIONS
--  (1) a = Nat -> a0 -> a1 (from f::a, eq.1, lhs)
--  (2) a = a3 -> a4 -> a1 (from f::a, eq.2, lhs)
--  (3) a0 = Nat -> a1 (from x::a0, eq.1, rhs)
--  (4) Nat -> Nat = a2 -> a3 (from ::a3, eq.2, lhs)
--  (5) a = a2 -> a4 -> a1 (from f::a, eq.2, rhs)

-- RESOLUTION OF RESTRICTIONS
--  from (1), (2) and (5) => Nat -> a0 -> a1 = a3 -> a4 -> a1 = a2 -> a4 -> a1
--  decomposing the previous => Nat = a3 = a2, a0 = a4
--  applying the decomposition to (4) => Nat -> Nat = a2 -> a2 = a3 -> a3
--  decomposing (4) => a2 = a3 = Nat
--  new set of equations:
--    (1) a = Nat -> a0 -> a1
--    (2) a = Nat -> a4 -> a1
--    (3) a0 = Nat -> a1
--    (4) Nat -> Nat = Nat -> Nat
--    (5) a = Nat -> a4 -> a1
--  eliminating (4) and (5) => {(1), (2), (3)}
--  unifying (2) => a = Nat -> a0 -> a1 -> We can eliminate it
--  new set:
--    (1) a = Nat -> a0 -> a1
--    (3) a0 = Nat -> a1
--  substituting a0 in (1) => a = Nat -> (Nat -> a1) -> a1

-- TYPE GENERALIZATION
--  Solved form: f:: Nat -> (Nat -> a1) -> a1


-- 2)
-- Given the following type
data Rational'' = Rational'
-- a/ Define the operation
simp :: Rational' -> Rational' -- simplifies the fraction (e.g. simp (Frac 18 6) = Frac 3 1)
-- simp (Frac x 0) = undefined
simp (Frac x y) =
  let divisor = gcd x y -- Greatest common divisor, can be 1
  in adjustSign (Frac (div x divisor) (div y divisor))

adjustSign :: Rational' -> Rational'
adjustSign (Frac x y)
  | (y == 0) = error "ERROR: Division by 0"
  | (y < 0) = (Frac (-x) (-y))
  | otherwise = (Frac x y)

-- b/ Define the sum of rationals, such that the result is simplified
sumsim :: Rational' -> Rational' -> Rational'
sumsim (Frac x0 y0) (Frac x1 y1) = simp (Frac (x0*y1+x1*y0) (y0*y1))

-- c/ Define rational as an instance of the class Ord
data Rational' = Frac Int Int deriving (Show)

instance Eq Rational' where
 frac == frac' = (x, y) == (x', y') -- what are x, x', y, y'?
   where Frac x y  = simp frac -- We bind (x, y) to the SIMPLIFICATION of frac
         Frac x' y' = simp frac'

instance Ord Rational' where
 frac <= frac' = (x, y) <= (x', y') -- Tuple comparison is in the standard
   where Frac x y = simp frac
         Frac x' y' = simp frac'

-- 3)
-- Define the following function in HASKELL
-- prefs xs = list of all prefixes of xs
-- (e.g. prefs [1,2,3] = [[],[1],[1,2],[1,2,3]])

--prefs :: [a] -> [[a]]
prefs [] = [[]]
prefs (x:xs) = [] : [x:ys | ys <- prefs xs]
-- prefs [1, 2] =
-- prefs 1:[2] =
-- [] : [1:ys | ys <- prefs [2]] =
-- [] : [1:ys | ys <- prefs 2:[]] =
-- [] : [1:ys | ys <- [] : [2:ys1 | ys1 <- prefs []]] =
-- [] : [1:ys | ys <- [] : [2:ys1 | ys1 <- [[]]]] =
-- [] : [1:ys | ys <- [] : [2:[]]] =
-- [] : [1:ys | ys <- [] : [[2]]] =
-- [] : [1:ys | ys <- [[], [2]] ] =
-- [] : [[1], [1, 2]] =
-- [[], [1], [1, 2]]

prefsWrong [] = [[]]
prefsWrong (x:xs) = [x:ys | ys <- prefs xs]
-- prefsWrong [1, 2, 3] =
-- prefsWrong 1:[2, 3] =
-- [1:ys | ys <- prefsWrong [2, 3]] =
-- [1:ys | ys <- prefsWrong 2:[3]] =
-- [1:ys | ys <- [2: ys1 | ys1 <- prefsWrong [3]]] =
-- [1:ys | ys <- [2: ys1 | ys1 <- [3:ys2 | ys2 <- prefsWrong []]]] =
-- [1:ys | ys <- [2: ys1 | ys1 <- [3:[]]]] =
-- [1:ys | ys <- [2: ys1 | ys1 <- [[3]]] =
-- [1:ys | ys <- [2: [3]]] =
-- [1:ys | ys <- [[2,3]]] =
-- [1:[2,3]] =
-- [[1,2,3]]

-- 5)
