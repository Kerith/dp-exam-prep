1. b -- OK!
2. b ?? Las respuestas dicen C, pero en g'' y no está definida
3. a -- X
4. a -- OK!
5. a -- OK!
6. b -- OK!
7. a -- OK!
8. b, dada la forma en la que construya listas haskell, tiene que ir mirando tails, lo que no termina
9. c, la primera es obvia y la segunda termina porque tail x = filter (/=2) x, y se tansforma en la primera.
10. b, si queremos el segundo elemento necesitamos construir la lista. -- OK!
11. a, vease 9 -- OK!
12. c, y no está definida -- OK!
13. b, en e4 y e6 (x undefined) -- OK!
14. a -- OK!
15. a, la primera (Bool != []). La tercera es correcta porque puede ser una lista de listas. -- OK!
16. b, la segunda está mal tipada y la primera y la tercera son equivalentes ([[True]]) -- OK!
17. a, la segunda está mal tipada. -- OK!
18. a, la primera, tercera y cuarta -- X (it's b)
19. b, la segunda y la cuarta -- OK!
20. a, la cuarta (1:2 está mal) -- OK!
21. a, la primera -- X (la tercera tambien esta mal)
22. b, segunda y tercera (resulta que 0:1:2 esta mal tambien, pero 0:1:2:[] no, parece que _enlaza por la derecha_) -- X
23. a -- OK!
24. c, la sublista [1, []] está mal tipada -- OK!
25. b -- OK!
26. a -- OK!
27. c, e no termina porque g no puede adivinar el valir de f 1\. e' si termina porque no lo necesita. -- OK!
28. c. -- OK!
29. b, la segunda, cuarta y quinta -- OK!
30. c, una vez te pones a evaluar f (x-1), entras en f 0 = f 0 -- OK!
31. a, g es tanto la variable que se le pasa a f como el nombre de la funcion. Context! -- OK!
32. b ?? -- OK!
33. b, no puede hacer operaciones de tipos, asi que no puede cambiar el resultado -- OK!
34. a, puede chequear equalidad -- OK!
35. b, si se puede conseguir bottom con computos infinitos -- OK!
36. a, si g mira en f, no acaba, y si no mira en f no puede dar undefined en la derecha -- OK!
37. b, asume que f mira en g loop -- OK!
38. a -- OK!
39. a -- OK!
40. a -- OK!
41. a -- OK!
42. c -- OK!
43. c -- X (es la a)
44. c, ultima x sin definir -- OK!
45. c, recursivo infinito -- OK!
46. c, recursivo infinito -- OK!
47. b -- OK!
48. a -- OK!
49. c, infinito -- OK!
50. b -- OK!
51. c -- OK!
52. b -- X, it's a. (a -> a) -> (a -> a) == (a -> a) -> a -> a
53. b -- OK! (a -> a -> a -> a) /= (a -> a) -> a -> a
54. c -- OK! ((a -> a) -> a) /= (a -> (a -> a))
55. a -- OK!
56. c -- OK!
57. b -- OK!
58. b -- OK!
59. c -- OK!
60. a -- X (era la b, (y^) no es nada xD)
61. b -- OK!
62. c -- X (era la b, porque (/y) 2 == 2/y!!!)
63. a -- OK!
64. c -- OK!
65. b -- OK!
66. ?? era la a... pues ok
67. c -- OK!
68. a -- OK!
69. c -- OK!
70. c, asumimos que : tiene poca prioridad -- OK!
71. a, y es el acumulador, asumimos que f x y es en realidad g x y -- OK!
72. a, porque en el foldl el acc está al otro lado -- OK!
73. a, el acumulador es y -- X, porque aunque el acc está bien, foldl coge los elementos de la izda.
74. c, f' multiplica ANTES de filtrar -- OK!
75. b, primero coge el 1, y hace acc = 1:1:[], luego el 2 y acc = 2:2:[1,1]... -- OK!
76. b ? No estoy seguro de lo que hace zipWith -- OK!
77. c -- OK!
78. c, i no está definido en j <- [...] -- OK!
79. a, si coges los numeros [1, 2, 3, 4, 5], a los unicos que les puedes sumar un y son <5 son [1, 2 ,3] -- X, resulta que como como para i=2, j <- [1, 2] y ambas valen, i=2 se repite
80. b, 1 para i = 3, 3 para i = 4, 3 para i = 5 -- OK!
81. b, 2 para cada i entre 3 y 5 -- OK!
82. b, i=4, j=i-1=3 -- OK!
83. b, porque por cada i solo hay un j valido, y hay 3 ies -- OK!
84. c, 0-3, 2-3, 1-(-1) -- OK!
85. a, 2/1, 4/2, 8/2 -- OK!
86. b, 1-1, 2-0, 2-3 -- OK!
87. b -- OK!
88. b, 2 por cada i -- OK!
89. c, zip acepta listas y map le pasa elementos -- OK!
90. a, map mapea length a [[...], [...], [...]] -- X, está mal tipada porque ys no es una lista, sino los elementos uno a uno del resultado de iterate (+3) x
91. wtf??
92. b, porque coge primero los de la sublista y antes de pasar al siguiente x
93. again, wtf?
94. a, por ejemplo la suma. Si es asociativa, el orden no importa -- X!!
95. a, n=3, m=0 (foldr devuelve solo el acc, que es 0) -- OK!
96. c, i no está definida fuera de la lista -- OK!
97. c? -- OK! 98.
