-- Junio 2014

-- 1)
--    a/ Write an expressio nin Haskell whose evaluation returns the list of
--       perfect squares between 100 and 2000
squares :: Int -> Int -> [Int]
squares a b = [x | x <- [a..b], square x]

square :: Int -> Bool
square x = length [y | y <- [1..(x`div`2)], y*y == x] >= 1

--    b/ What is the type of the following function? Give reasons for your answer
--f x y = x : f x (y x)

-- DECORATION:
--(f::a x::a0 y::a1)::a2 = (x::a0 : [(f::a x::a0 (y::a1 x::a0)::a4)::a3])::a2

-- GENERATE RESTRICTIONS:
--  (1) a = a0 -> a1 -> a2
--  (2) a0 : [a3] -> a2
--  (3) a = a0 -> a4 -> a3
--  (4) a1 = a0 -> a4

-- SOLVE RESTRICTIONS:
-- from unification of (1) and (3) => a1 = a4, a3 = a2
-- delete (3)
-- substituting (4) in (1) => a = a0 -> (a0 -> a4) -> a2

-- WRONGLY TYPED!!

-- 2)
-- Supossing the following Haskell Type
data Arbol a = Nodo a [Arbol a] deriving Eq
--   a/ Code the following functions, indicating their type
--      i/ binario t returns true iff the tree t is binary
binario :: (Eq a) => Arbol a -> Bool
binario (Nodo a as) = length as <= 2 && (filter (binario) as) == as
--      ii/ nodos t returns the number of nodes of t
-- The number of nodes of a tree is 1 + the number of nodes on it's children
nodos :: (Eq a) => Arbol a -> Int
nodos (Nodo a as) = 1 + sum (map (nodos) as)

--   b/ Define Arbol a as an instance of the class Ord, according to the following rules:
--      t0 < t1 iff t0 has less nodes than t1 or, having the same number of nodes,
--      a0 < a1, being a0 and a1 the roots of t0 and t1
instance (Ord a) => Ord (Arbol a) where
  t0 <= t1 = t0 == t1 || t0 < t1
  t0 < t1 = n0 < n1 || n0 == n1 && raiz t0 < raiz t1
    where  n0 = nodos t0
           n1 = nodos t1
           raiz (Nodo a _) = a

-- 3) Define, indicating the types, the following functions,
--    using foldl or folr at least once
--
--    a/ repetir xs = result of repeating each element x of xs x times
-- The result is the concatenation of all the lists that result from
-- Repeating x x times.
repetir :: [Int] -> [Int]
repetir xs = [ys | x <- xs, ys <- take x [x, x..]]
--    b/ cuenta x xs = number of times that x appears in the list xs
cuenta :: (Eq a) => a -> [a] -> Int
cuenta x xs = foldr (\e acc -> if e == x then acc+1 else acc) 0 xs
